import React,{Component} from 'react'

class Heading extends Component{
	constructor(props){
		super(props)

	}

	render(){
		return(
			<div className="Heading-Container">
				<div id="parHead" className="Heading-BG">
				</div>
				<div className="row Heading-Head">
					- UptoDate -
				</div>
				<div className="row Heading-SubHead">
					Get in Touch with our simple News Feed.
				</div>
			</div>
		);
	}
}

export default Heading;